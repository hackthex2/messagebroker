package com.monocept.auditing.entity;


public class Request {
	
	private AuditDetails auditDetails;
	
	public AuditDetails getAuditDetails() {
		return auditDetails;
	}
	public void setAuditDetails(AuditDetails auditDetails) {
		this.auditDetails = auditDetails;
	}
	@Override
	public String toString() {
		return "Request [auditDetails=" + auditDetails + "]";
	}

	
}

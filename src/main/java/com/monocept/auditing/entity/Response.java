package com.monocept.auditing.entity;

import org.springframework.http.HttpStatus;

public class Response {
	
	private Boolean status;
	private String message;
	private ErrorMessage errorMessage;
	private HttpStatus statusCode;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", errorMessage=" + errorMessage
				+ ", statusCode=" + statusCode + "]";
	}

	

}

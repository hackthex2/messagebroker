package com.monocept.auditing.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "auditDetails")
public class AuditDetails {

	    @Id
	    @JsonProperty("id")
	    private String id;
	    
	    @JsonProperty("requestId")
	    private Long requestId;
	    
	    @JsonProperty("taskDetails")
	    private TaskDetails taskDetails;
	
	    @JsonProperty("Status")
	    private String status;
	  
	    @JsonProperty("createdTime")
	    private Date createdTime;

	    @JsonProperty("updatedTime")
	    private Date updatedTime;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Long getRequestId() {
			return requestId;
		}

		public void setRequestId(Long requestId) {
			this.requestId = requestId;
		}

		public TaskDetails getTaskDetails() {
			return taskDetails;
		}

		public void setTaskDetails(TaskDetails taskDetails) {
			this.taskDetails = taskDetails;
		}

	

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getCreatedTime() {
			return createdTime;
		}

		public void setCreatedTime(Date createdTime) {
			this.createdTime = createdTime;
		}

		public Date getUpdatedTime() {
			return updatedTime;
		}

		public void setUpdatedTime(Date updatedTime) {
			this.updatedTime = updatedTime;
		}

		@Override
		public String toString() {
			return "AuditDetails [id=" + id + ", requestId=" + requestId + ", taskDetails=" + taskDetails + ", status="
					+ status + ", createdTime=" + createdTime + ", updatedTime=" + updatedTime + "]";
		}

		
	    
	    
}

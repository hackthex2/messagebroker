package com.monocept.queue.response;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.monocept.queue.entity.DashboardCount;
import com.monocept.queue.entity.DashboardDetails;
import com.monocept.queue.entity.MasterDetails;

public class Response {
	
	private Boolean status;
	private String message;
	private ErrorMessage errorMessage;
	private HttpStatus statusCode;
	private MasterDetails masterDetails;
	private DashboardDetails dashboardDetails;
	List<DashboardCount> dashboardResult;
	
	
	
	public List<DashboardCount> getDashboardResult() {
		return dashboardResult;
	}
	public void setDashboardResult(List<DashboardCount> dashboardResult) {
		this.dashboardResult = dashboardResult;
	}
	public DashboardDetails getDashboardDetails() {
		return dashboardDetails;
	}
	public void setDashboardDetails(DashboardDetails dashboardDetails) {
		this.dashboardDetails = dashboardDetails;
	}
	public MasterDetails getMasterDetails() {
		return masterDetails;
	}
	public void setMasterDetails(MasterDetails masterDetails) {
		this.masterDetails = masterDetails;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", errorMessage=" + errorMessage
				+ ", statusCode=" + statusCode + ", masterDetails=" + masterDetails + ", dashboardDetails="
				+ dashboardDetails + ", dashboardResult=" + dashboardResult + "]";
	}
	
	
	

}

package com.monocept.queue.response;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class OutputResponse {
	
	private Response response;
	
	
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "OutputResponse [response=" + response + "]";
	}

	
	
	
	
}

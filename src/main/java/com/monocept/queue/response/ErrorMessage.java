package com.monocept.queue.response;

import java.util.List;

public class ErrorMessage {
	
	List<Object> errorMessage;

	public List<Object> getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(List<Object> errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "ErrorMessage [errorMessage=" + errorMessage + "]";
	}
	
	

}

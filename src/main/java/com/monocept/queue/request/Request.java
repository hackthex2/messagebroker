package com.monocept.queue.request;

import com.monocept.queue.entity.MasterDetails;

public class Request {
	
	private MasterDetails masterDetails;

	public MasterDetails getMasterDetails() {
		return masterDetails;
	}

	public void setMasterDetails(MasterDetails masterDetails) {
		this.masterDetails = masterDetails;
	}

	@Override
	public String toString() {
		return "Request [masterDetails=" + masterDetails + "]";
	}
	
	

}

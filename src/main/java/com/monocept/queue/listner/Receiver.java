package com.monocept.queue.listner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.monocept.queue.controller.MessageBrokerController;
import com.monocept.queue.request.InputRequest;
import com.monocept.queue.service.DashboardServiceProxy;

@Component
public class Receiver {
	
	private static final Logger logger = LoggerFactory.getLogger(Receiver.class);
	
	@Autowired
	DashboardServiceProxy dashboardServiceProxy;

	@JmsListener(destination = "dataProcessingStream", containerFactory = "myFactory")
	public void receiveMessage(InputRequest inputRequest) {
		Long requestId=inputRequest.getRequest().getMasterDetails().getRequestId();
		logger.info("sending request to data processing service {} for requestId {} ",requestId);
		ResponseEntity<Object> response=dashboardServiceProxy.save(inputRequest);
		
		logger.info("Response received  for requestId {} and response is {} ",requestId ,response.getBody());
	}

}

package com.monocept.queue.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class MasterDetails {
	
	private String id;
	private Long requestId;
	private String finalStatus;
	private PaymentDetails paymentDetails;
	private EmailDetails emailDetails;
	private UserDetails userDetails;
	private TaskDetails taskDetails;
	private Integer priority;
	private Date startTime;
	private Date endTime;
	
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getRequestId() {
		return requestId;
	}
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
	public String getFinalStatus() {
		return finalStatus;
	}
	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public EmailDetails getEmailDetails() {
		return emailDetails;
	}
	public void setEmailDetails(EmailDetails emailDetails) {
		this.emailDetails = emailDetails;
	}
	public UserDetails getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	public TaskDetails getTaskDetails() {
		return taskDetails;
	}
	public void setTaskDetails(TaskDetails taskDetails) {
		this.taskDetails = taskDetails;
	}
	@Override
	public String toString() {
		return "MasterDetails [id=" + id + ", requestId=" + requestId + ", finalStatus=" + finalStatus
				+ ", paymentDetails=" + paymentDetails + ", emailDetails=" + emailDetails + ", userDetails="
				+ userDetails + ", taskDetails=" + taskDetails + ", priority=" + priority + ", startTime=" + startTime
				+ ", endTime=" + endTime + "]";
	}
	
	
}

package com.monocept.queue.entity;

public class UserDetails {
	
	private String name;
	private String contact;
	private String address;
	private String city;
	private String district;
	private String state;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", contact=" + contact + ", address=" + address + ", city=" + city
				+ ", district=" + district + ", state=" + state + "]";
	}
	
}

package com.monocept.queue.entity;

public class DashboardCount {
	
	
	private int total;
	private String name;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "DashboardCount [total=" + total + ", name=" + name + "]";
	}
	

}

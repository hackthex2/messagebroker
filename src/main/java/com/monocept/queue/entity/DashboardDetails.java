package com.monocept.queue.entity;

import java.util.Map;

public class DashboardDetails {
	
	private Map<String,Double> dashboardCount;

	public Map<String, Double> getDashboardCount() {
		return dashboardCount;
	}

	public void setDashboardCount(Map<String, Double> dashboardCount) {
		this.dashboardCount = dashboardCount;
	}

	@Override
	public String toString() {
		return "DashboardDetails [dashboardCount=" + dashboardCount + "]";
	}
	
	

}

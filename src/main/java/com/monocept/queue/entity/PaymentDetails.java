package com.monocept.queue.entity;

public class PaymentDetails {
	
	private String accountNumber;
	private String ifscCode;
	private String bankName;
	private String branchName;
	private String status;
	private String accountHolderName;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	@Override
	public String toString() {
		return "PaymentDetails [accountNumber=" + accountNumber + ", ifscCode=" + ifscCode + ", bankName=" + bankName
				+ ", branchName=" + branchName + ", status=" + status + ", accountHolderName=" + accountHolderName
				+ "]";
	}
	
}

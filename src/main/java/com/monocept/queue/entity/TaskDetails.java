package com.monocept.queue.entity;

public class TaskDetails {
	
	private String name;
    private String status;
    private String taskId;
    private Integer failCount;
    
    
	public Integer getFailCount() {
		return failCount;
	}
	public void setFailCount(Integer failCount) {
		this.failCount = failCount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaskDetails [name=" + name + ", status=" + status + ", taskId=" + taskId + ", failCount=" + failCount
				+ "]";
	}
	
    
    
}

package com.monocept.queue.entity;

public class EmailDetails {
	
	private String email;
    private String status;
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "EmailDetails [email=" + email + "]";
	}
	
	
	


	

}

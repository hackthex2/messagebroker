package com.monocept.queue.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.monocept.queue.request.InputRequest;

@FeignClient(name="dashboard-service")
@Service
@RibbonClient(name="dashboard-service")
public interface DashboardServiceProxy {
	
	   @RequestMapping(value = "/fulfillment/api/save",method = RequestMethod.POST)
	   public ResponseEntity<Object> save(@RequestBody InputRequest inputRequest);

}

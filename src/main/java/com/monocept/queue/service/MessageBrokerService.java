package com.monocept.queue.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.monocept.queue.request.InputRequest;


@Service
public class MessageBrokerService {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(MessageBrokerService.class);
	
	@Async
	public void saveData(InputRequest inputRequest)
	{
		long requestId=inputRequest.getRequest().getMasterDetails().getRequestId();
		logger.info("data recieved in service for requestId {}",requestId);
		jmsTemplate.convertAndSend("dataProcessingStream", inputRequest);
		logger.info("data pushed to strema  for requestId {}",requestId);
	}

}

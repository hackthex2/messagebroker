package com.monocept.queue.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.monocept.queue.request.InputRequest;
import com.monocept.queue.service.MessageBrokerService;

@RestController
@CrossOrigin
public class MessageBrokerController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageBrokerController.class);
	
	@Autowired
	MessageBrokerService messageBrokerService;
	
	@PostMapping(value = "/masterDetails")
	public ResponseEntity<Object> masterDetails(@RequestBody InputRequest inputRequest)
	{
		long requestId=inputRequest.getRequest().getMasterDetails().getRequestId();
		logger.info("request data recieved in stream for requestId {}",requestId);
		messageBrokerService.saveData(inputRequest);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
